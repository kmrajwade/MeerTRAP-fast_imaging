#!/usr/env/python

# A script to generate fake xGPU data to feed to the library

# Kaustubh Rajwade

import numpy as np
import sys,os


def usage():
    print("python gen_data.py <number of channels> <number of time samples> <number of antennas>")
    print ("Note: The number of antennas should be less than 16 (xGPU library constraints)")

# Make sure we have the right arguments

if (len(sys.argv) != 4):
    usage()
    raise Exception("Incorrect number of Arguments")

# parse arguments
num_chans = int(sys.argv[1])   # Number of channels
num_samps = int(sys.argv[2])   # Number of time samples
num_ants = int(sys.argv[3])    # Number of antennas

# Opening file for output

f = open("random_data.dat","wb")

# defining boundaries

complexity = 2
npol = 2

# generating data
for i in range(num_samps):
    
    for j in range(num_chans):
        
        data = np.random.randint(1, 255,num_ants*npol*complexity,'uint8')
        data_rem = np.zeros((16-num_ants)*npol*complexity,dtype='uint8')
        data_final = np.concatenate([data,data_rem])
        data_final.tofile(f,sep="",format="%B")

#closing file

f.close()
