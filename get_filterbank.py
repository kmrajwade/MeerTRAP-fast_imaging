#!/usr/env/python

import numpy as np
import matplotlib.pyplot as plt
import sys,os
import struct

# setting up the filterbank header for SIGPROC format

telescope_ids = {"Fake": 0, "Arecibo": 1, "Ooty": 2, "Nancay": 3,
                 "Parkes": 4, "Jodrell": 5, "GBT": 6, "GMRT": 7,
                 "Effelsberg": 8, "MeerKAT": 9}

machine_ids = {"WAPP": 0, "PSPM": 1, "Wapp": 2,"AOFTM": 3,
               "BCPM1": 4, "FLAGBF": 5, "SCAMP": 6,
               "GBT Pulsar Spigot": 7, "SPIGOT": 7, "PUPPI": 8 ,
               "GUPPI": 9,"PA":10,"VEGAS": 11}

# Section left blank to get header parameters from the file itself
def prep_string(string):
    return struct.pack('i', len(string)) + string

def prep_double(name, value):
    return prep_string(name)+struct.pack('d', float(value))

def prep_int(name, value):
    return prep_string(name)+struct.pack('i', int(value))

# Getting the necessary header parameters

hdr = prep_string("HEADER_START")
hdr += prep_int("telescope_id", 9)
hdr += prep_int("machine_id",0)
hdr += prep_int("data_type", 1) # 1 = filterbank, 2 = timeseries

source="PSR B0833-45"
hdr += prep_string("source_name")
hdr += prep_string(source)
hdr += prep_double("src_raj",083520.611)
hdr += prep_double("src_dej", -451034.87)
hdr += prep_int("nbits", 32)
hdr += prep_int("nifs", 4)
hdr += prep_int("nchans", 16)
hdr += prep_double("fch1",1421.09 )
hdr += prep_double("foff", -0.209)
hdr += prep_double("tstart", 56857.659)
hdr += prep_double("tsamp", 0.009570092)
hdr += prep_string("HEADER_END")

#check number of arguments
if (len(sys.argv) != 5):
    print ("Incorrect number of arguments!")
    print ("usage: python get_filterbank.py <path to xgpu file> <number of timestamps> <number of freq channels> <number of antennas>")
    exit()


#open input file
f = open(sys.argv[1],"rb")
data = np.fromfile(f,dtype=np.complex64)
f.close()

#how many timesamples to use
ts = int(sys.argv[2])

# Arguments 
nchans=int(sys.argv[3])
nants=int(sys.argv[4])

index1 = nants*(nants+2)/2
index2 = nants*nants

elems_per_mat = index2 + (2*index1)

spectra=[]
# get auto-correlations. For now hardwired to do only 1,1 antenna (dish1)
for i in range(ts):
   bpass = []

   for j in range(nchans):
     
      for k in range(4):
 
         AA = abs(data[elems_per_mat*j + elems_per_mat*nchans*i + k])
         bpass.append(AA)
   bandpass = np.array(bpass)
   spectra.append(bandpass[::-1])

spectrum = np.array(spectra,dtype="float32")

f1 = open("filterbank.dat","wb")

spectrum.tofile(f1,sep="",format="%f")

f1.close()

# Adding the header parameters to the file

basefilename = "filterbank.dat"
outfile = open(basefilename+".fil", "wb")
outfile.write(hdr)
outfile.close()
os.system("cat %s >> %s"%(basefilename, basefilename+".fil"))
