# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/krajwade/xgpu_test/tb_transpose/tb_transpose/src/simple_file_writer.cpp" "/home/krajwade/xgpu_test/tb_transpose/build/tb_transpose/CMakeFiles/tb_transpose.dir/src/simple_file_writer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/share/apps/include"
  "/usr/local/include"
  "../thirdparty/boost"
  "/home/krajwade/xgpu_test/cutt/include"
  "/usr/local/share/apps/xGPU/include"
  "../tb_transpose/.."
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
