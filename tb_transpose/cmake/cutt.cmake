IF (CUTT_INCLUDE_DIR)
    SET(CUTT_INC_DIR ${CUTT_INCLUDE_DIR})
    UNSET(CUTT_INCLUDE_DIR)
ENDIF (CUTT_INCLUDE_DIR)

FIND_PATH(CUTT_INCLUDE_DIR cutt.h 
    PATHS ${CUTT_INC_DIR}
    ${CUTT_INSTALL_DIR}/include
    /usr/local/include
    /usr/include )
message("Found ${CUTT_INCLUDE_DIR} : ${CUTT_INSTALL_DIR}")

SET(CUTT_NAMES)
FOREACH( lib ${CUTT_NAMES} )
    FIND_LIBRARY(CUTT_LIBRARY_${lib}
        NAMES ${lib}
        PATHS ${CUTT_LIBRARY_DIR} ${CUTT_INSTALL_DIR} ${CUTT_INSTALL_DIR}/lib /usr/local/lib /usr/lib
        )
    LIST(APPEND CUTT_LIBRARIES ${CUTT_LIBRARY_${lib}})
ENDFOREACH(lib)

    # handle the QUIETLY and REQUIRED arguments and set CUTT_FOUND to TRUE if.
    # all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CUTT DEFAULT_MSG CUTT_LIBRARIES CUTT_INCLUDE_DIR)

IF(NOT CUTT_FOUND)
    SET( CUTT_LIBRARIES )
    SET( CUTT_TEST_LIBRARIES )
ELSE(NOT CUTT_FOUND)
    # -- add dependecies
    LIST(APPEND CUTT_LIBRARIES ${CUDA_curand_LIBRARY})
ENDIF(NOT CUTT_FOUND)

LIST(APPEND CUTT_INCLUDE_DIR "${CUTT_INCLUDE_DIR}")

MARK_AS_ADVANCED(CUTT_LIBRARIES CUTT_TEST_LIBRARIES CUTT_INCLUDE_DIR)
