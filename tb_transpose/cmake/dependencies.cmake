include(cuda)
include(compiler_settings)
include(cmake/boost.cmake)
include(cmake/cutt.cmake)
include(cmake/xgpu.cmake)
include_directories(SYSTEM ${Boost_INCLUDE_DIR} ${CUTT_INCLUDE_DIR} ${XGPU_INCLUDE_DIR})
set(DEPENDENCY_LIBRARIES
    ${CUTT_LIBRARIES}
    ${Boost_LIBRARIES}
    ${XGPU_LIBRARIES}
    ${CUDA_CUDART_LIBRARY}
)
