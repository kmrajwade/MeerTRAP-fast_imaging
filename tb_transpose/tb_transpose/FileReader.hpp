#ifndef TB_TRANSPOSE_FILEREADER_HPP
#define TB_TRANSPOSE_FILEREADER_HPP


#pragma once

#include "raw_bytes.hpp"
#include "common.hpp"
#include <ostream>
#include <string>
#include <iostream>
#include <fstream>

// A templated binary file reader irrespective of type of data.


namespace tb_transpose
{

template<typename NumRepType, typename HandlerType>
class FileReader
{
public:
	FileReader(std::string fileName, HandlerType& handler, std::uint32_t nelements); // create array based on file with this name
	~FileReader(void);
	bool getStatus(void); // true means "good", false means "bad"
	char getItem(int index); // get item at position specified by index

	// proper use requires properly invoking the destructor
	NumRepType& operator[](int index); // overload for both get and set

    bool read(RawBytes<NumRepType>& block);

	std::streampos	fileSize(void);

    void start();

    void stop();


private:
    HandlerType& _handler;
    std::uint32_t _nelements;
    bool _stop;
    bool _running;
	std::fstream _file; // stream to/from array file
	bool _status; // file status



	// enable use of operator[] to set values in array
	NumRepType getValue(int index); // get value from file
	void setValue(int index, NumRepType value); // set item at position specified by index

    NumRepType _value; // operator[] returns reference to here


	// these variables support use of operator[] on the left side of an
	// assignment statement.
	// this requires proper invocation of the destructor, to deal with prevIndex
	int _prevIndex;
	NumRepType _prevValue;

};
}
#include "tb_transpose/detail/FileReader.cpp"
#endif
