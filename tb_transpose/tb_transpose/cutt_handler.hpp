#ifndef TB_TRANSPOSE_CUTT_HANDLER_HPP
#define TB_TRANSPOSE_CUTT_HANDLER_HPP


#pragma once

#include "raw_bytes.hpp"
#include "common.hpp"
#include <ostream>
#include <string>
#include <iostream>
#include <fstream>

// A templated binary file reader irrespective of type of data.


namespace tb_transpose
{

template<typename NumRepType, typename HandlerType>
class CuttHandler
{
public:
    CuttHandler(HandlerType& handler,std::string size_string, std::string trans_string, std::uint32_t nelements); // create array based on file with this name
	~CuttHandler(void);

    void init(RawBytes<char>& block);

    void operator()(RawBytes<NumRepType>& block);


private:
    HandlerType& _handler;
    std::string _size_string;
    std::string _trans_string;
    std::uint32_t _nelements;

};
}
#include "tb_transpose/detail/cutt_handler.cpp"
#endif
