#include <iostream>
#include <iomanip>
#include "tb_transpose/FileReader.hpp"

// flags for opening a file that already exists
static const std::ios::open_mode  	readWriteMode = std::ios::in | std::ios::out | std::ios::binary;

// flags for creating a file that does not yet exist
static const std::ios::open_mode	createMode = std::ios::out | std::ios::binary;

namespace tb_transpose
{

 template<typename NumRepType, typename HandlerType>
FileReader<NumRepType, HandlerType>::FileReader(std::string fileName, HandlerType& handler, std::uint32_t nelements)
: _handler(handler)
, _nelements(nelements)
, _stop(false)
, _running(false)
{
	_file.open(fileName.c_str(), std::ios::in | std::ios::binary); // open file in read/write mode
    if(!_file)
    {
        BOOST_LOG_TRIVIAL(error) << "Error opening File";
        throw;
    }
	_status = _file.is_open() && _file.good(); // record stream status
}

 template<typename NumRepType, typename HandlerType>
FileReader<NumRepType, HandlerType>::~FileReader(void)
{
    if (!_stop)
        _stop=true;

    if (_value != _prevValue)
        setValue(_prevIndex, _value);

    BOOST_LOG_TRIVIAL(info) << "Closing file";
    _file.close();
}

 template<typename NumRepType, typename HandlerType>
bool FileReader<NumRepType, HandlerType>::getStatus(void)
{
	return _status;
}

 template<typename NumRepType, typename HandlerType>
NumRepType FileReader<NumRepType, HandlerType>::getValue(int index)
{
	NumRepType	v;
	bool	eof = false;
	
	_file.seekg(index * sizeof(v));
	_file.read((char *) &v, sizeof(v));
	if (_file.eof()) {
		_file.clear(); // clear eof and fail error bits
		v = 0; // default value for Windows file system
		eof = true;
		}
	std::cout << "Value:\t" << (int) v << " <- file[" << index << "]";
	if (eof)
		std::cout << "     (eof)";
	std::cout << std::endl;
	return v;
}

 template<typename NumRepType, typename HandlerType>
void FileReader<NumRepType, HandlerType>::setValue(int index, NumRepType value)
{
	_file.seekp(index * sizeof(value));
	_file.write((char *) &value, sizeof(value));
	std::cout << "file[" << index << "] <- " << (int) value << std::endl;
}

// This operator[] works on the lefthand side of an assignment by
// storing values from the previous time it was called. This allows
// it to discover if it was called on the lefthand side by comparing
// the data in the 'value' data member to what it put into 'value'
// the previous time. If that has changed, then the previous call
// was on the lefthand side and the file needs to be updated.
 template<typename NumRepType, typename HandlerType>
NumRepType& FileReader<NumRepType, HandlerType>::operator[](int index)
{
	std::cout << *this;
	// write out previously set value to file if necessary
	if (_value != _prevValue)
		setValue(_prevIndex, _value);

	_value = getValue(index);

	// record current values for next time
	_prevIndex = index;
	_prevValue = _value;
	std::cout << std::endl;
	return _value;
}


template<typename NumRepType, typename HandlerType>
void FileReader<NumRepType, HandlerType>::start()
{
    bool handler_stop_request = false;
    if (_running)
    {
        throw std::runtime_error("Stream is already running");
    }
    _running=true;


    // Pass Header information
    char* hdr_ptr = new char[4096];
    _file.read(hdr_ptr, 4096);
    RawBytes<char> header_block(hdr_ptr, 4096, 0, false);
    _handler.init(header_block);
    delete[] hdr_ptr;

// read the header (assumes PSRDADA ascii header format) 
    while(!_stop && !handler_stop_request)
    {
        
        // Get data now
        std::vector<NumRepType> data(_nelements);
        RawBytes<NumRepType> data_block(data.data(), _nelements,0, false);
        handler_stop_request = read(data_block);
        if (handler_stop_request)
        {
            _running = false;
            return;
        }
        _handler(data_block);
    }
    _running=false;
}

template<typename NumRepType, typename HandlerType>
void FileReader<NumRepType, HandlerType>::stop()
{
    _stop=true;
}

template<typename NumRepType, typename HandlerType>
std::streampos FileReader<NumRepType, HandlerType>::fileSize(void)
{
	_file.seekg(0, std::ios::end); // seek to end of file
	return _file.tellg(); // find out where that is
}

template<typename NumRepType, typename HandlerType>
bool FileReader<NumRepType, HandlerType>::read(RawBytes<NumRepType>& block)
{
    if(_status)
    {
        BOOST_LOG_TRIVIAL(info) << "Reading " << _nelements << " elements of size " << sizeof(NumRepType);
        _file.read( (char *) block.ptr(), _nelements * sizeof(NumRepType));

        if (_file.eof()) 
        {
            BOOST_LOG_TRIVIAL(info) << "Reached end of file!";
            return true;
        }
        return false;
    }
    else
    {
        BOOST_LOG_TRIVIAL(error) <<  "Error on read";
        return true;
    }
}

}
