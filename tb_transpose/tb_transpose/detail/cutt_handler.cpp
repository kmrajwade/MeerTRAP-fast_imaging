#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include "tb_transpose/common.hpp"
#include "boost/algorithm/string.hpp"
#include "tb_transpose/FileReader.hpp"
#include "cutt.h"
#include "boost/program_options.hpp"

#define HANDLE_ERROR(x) \
{ \
	cudaError_t err = x; \
	if( cudaSuccess != err ) \
	{ \
		fprintf( stderr, \
		         "CUDA Error on call \"%s\": %s\n\tLine: %d, File: %s\n", \
		         #x, cudaGetErrorString( err ), __LINE__, __FILE__); \
		fflush( stdout );\
		exit( 1 ); \
	} \
}

#define cuttCheck(stmt) do {                                 \
  cuttResult err = stmt;                            \
  if (err != CUTT_SUCCESS) {                          \
    fprintf(stderr, "%s in file %s, function %s\n", #stmt,__FILE__,__FUNCTION__); \
    exit(1); \
  }                                                  \
} while(0)



using namespace tb_transpose;

template<typename NumRepType, typename HandlerType>
CuttHandler<NumRepType, HandlerType>::CuttHandler(HandlerType& handler, std::string size_string, std::string trans_string, std::uint32_t nelements)
: _handler(handler)
, _size_string(size_string)
, _trans_string(trans_string)
, _nelements(nelements)
{
}

template<typename NumRepType, typename HandlerType>
CuttHandler<NumRepType, HandlerType>::~CuttHandler()
{
}

template<typename NumRepType, typename HandlerType>
void CuttHandler<NumRepType, HandlerType>::init(RawBytes<char>& block)
{
    _handler.init(block);
}

template<typename NumRepType, typename HandlerType>
void CuttHandler<NumRepType, HandlerType>::operator()(RawBytes<NumRepType>& block)
{    
    BOOST_LOG_TRIVIAL(info) << "Starting Transpose....";
    std::vector<std::string> vsize;
    std::vector<int> visize;
    boost::split( vsize, _size_string, boost::is_any_of("x"));
    std::vector<std::string> vdirection;
    std::vector<int> vidirection; 
    boost::split( vdirection, _trans_string, boost::is_any_of(","));

    std::transform(vsize.begin(), vsize.end(), std::back_inserter(visize),
               [](const std::string& str) { return std::stoi(str); });

    std::transform(vdirection.begin(), vdirection.end(), std::back_inserter(vidirection),
               [](const std::string& str) { return std::stoi(str); });

    std::ofstream outfile;

    /*outfile.open("transposed_pipeline_test.dat",std::ifstream::out | std::ifstream::binary | std::ifstream::app);
    if (outfile.is_open())
    {
        BOOST_LOG_TRIVIAL(debug) << "Opened file transposed_pipeline_test.dat" ;
    }
    else
    {
        std::stringstream stream;
        stream << "Could not open file transposed_pipeline_test.dat";
        throw std::runtime_error(stream.str().c_str());
    }*/

    int* size = &visize[0];
    int* direction = &vidirection[0];
   
    //BOOST_LOG_TRIVIAL(info) << "Sizes are: " << size[0] << " " << size[1] << " " << size[2] << " " << size[3]; 
    // Define arrays
    float * dev_in_array,
          * dev_out_array;

    std::vector<NumRepType> out_array_new(_nelements);
    std::vector<float> out_array(_nelements);
    std::vector<float> in_array(_nelements);
    //allocate memory

    HANDLE_ERROR( cudaMalloc( &dev_in_array, _nelements * sizeof( float ) ) ); 
    HANDLE_ERROR( cudaMalloc( &dev_out_array, _nelements * sizeof( float ) ) ); 

    try
    {
        for (std::uint32_t jj=0 ; jj < _nelements; ++jj)
        {
            in_array[jj] = (float) (block.ptr()[jj]);
        }

        // Transfer data to the device memory and run transpose

        HANDLE_ERROR( cudaMemcpy( dev_in_array,
                    in_array.data(),
                    _nelements * sizeof(float),
                    cudaMemcpyHostToDevice ) ); 


        // run the transpose
        cuttHandle plan;
        cuttCheck(cuttPlan(&plan, visize.size(), size, direction, sizeof(float), 0)); 

        cuttCheck(cuttExecute(plan, dev_in_array, dev_out_array));

        // copy output to CPU
        HANDLE_ERROR(cudaMemcpy( out_array.data(), dev_out_array, _nelements * sizeof(float), cudaMemcpyDeviceToHost ));

        //destroy plan
        cuttDestroy(plan);

        // Convert to NumReptype
        for (std::uint32_t jj =0; jj < _nelements; ++jj)
        {
            out_array_new[jj] = static_cast<NumRepType>(out_array[jj]);
        }

       // outfile.write(reinterpret_cast<const char*>(out_array_new.data()), _nelements*sizeof(NumRepType));

        // Pass to the next handler
        RawBytes<NumRepType> transposed(out_array_new.data(), _nelements*sizeof(NumRepType), 0, false);
        _handler(transposed);
        //outfile.close();

    }
    catch (...)
    {
        HANDLE_ERROR(cudaFree(dev_out_array));
        HANDLE_ERROR(cudaFree(dev_in_array));
        throw;
    }
// free all arrays
    BOOST_LOG_TRIVIAL(info) << "Deallocating tranpose arrays...";
    HANDLE_ERROR(cudaFree(dev_out_array));
    HANDLE_ERROR(cudaFree(dev_in_array));
}
