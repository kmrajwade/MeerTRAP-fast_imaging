#include "tb_transpose/raw_bytes.hpp"

namespace tb_transpose
{

template<typename NumRepType>
RawBytes<NumRepType>::RawBytes(NumRepType* ptr, std::size_t total, std::size_t used, bool device_memory)
    : _ptr(ptr)
    , _total_bytes(total)
    , _used_bytes(used)
    ,_on_device(device_memory)
{
}

template<typename NumRepType>
RawBytes<NumRepType>::~RawBytes()
{
}

template<typename NumRepType>
std::size_t RawBytes<NumRepType>::total_bytes() const
{
    return _total_bytes;
}

template<typename NumRepType>
std::size_t RawBytes<NumRepType>::used_bytes() const
{
    return _used_bytes;
}

template<typename NumRepType>
void RawBytes<NumRepType>::used_bytes(std::size_t used)
{
    _used_bytes = used;
}

template<typename NumRepType>
NumRepType* RawBytes<NumRepType>::ptr()
{
    return _ptr;
}

template<typename NumRepType>
bool RawBytes<NumRepType>::on_device() const
{
    return _on_device;
}

}
