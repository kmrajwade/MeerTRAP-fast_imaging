#include "tb_transpose/simple_file_writer.hpp"

namespace tb_transpose {

    template<typename NumRepType>
    void SimpleFileWriter::operator()(RawBytes<NumRepType>& block)
    {
        try
        {
            BOOST_LOG_TRIVIAL(info) << "Writing " << _nelements*sizeof(NumRepType) << " bytes to file";
            _outfile.write(reinterpret_cast<const char*>(block.ptr()), _nelements*sizeof(NumRepType));
            BOOST_LOG_TRIVIAL(info) << "Written " << _nelements*sizeof(NumRepType) << " bytes to file";
        }
        catch (...)
        {
            throw;
        }
    }

} //psrdada_cpp
