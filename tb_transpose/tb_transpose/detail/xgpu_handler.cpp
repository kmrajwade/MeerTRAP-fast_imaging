#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cassert>
#include <new>
#include "boost/algorithm/string.hpp"
#include "tb_transpose/common.hpp"
#include "tb_transpose/xgpu_handler.hpp"

#define HANDLE_ERROR(x) \
{ \
	cudaError_t err = x; \
	if( cudaSuccess != err ) \
	{ \
		fprintf( stderr, \
		         "CUDA Error on call \"%s\": %s\n\tLine: %d, File: %s\n", \
		         #x, cudaGetErrorString( err ), __LINE__, __FILE__); \
		fflush( stdout );\
		exit( 1 ); \
	} \
}


using namespace tb_transpose;

template<typename NumRepType, typename HandlerType>
XgpuHandler<NumRepType, HandlerType>::XgpuHandler(HandlerType& handler, std::uint32_t tsamples, std::uint32_t nantennas)
: _handler(handler)
, _tsamples(tsamples)
, _nantennas(nantennas)
{
    auto device =0 ;
    int xgpu_error=0;
    xgpuInfo(&_info);
    _context.array_h = NULL;
    _context.matrix_h = NULL;

    xgpu_error = xgpuInit(&_context, device);
    if(xgpu_error) 
    {
        BOOST_LOG_TRIVIAL(error) << "xgpuInit returned error code: " << xgpu_error;
        throw;
    }
}

template<typename NumRepType, typename HandlerType>
XgpuHandler<NumRepType, HandlerType>::~XgpuHandler()
{
    // free gpu memory
    xgpuFree(&_context);
    free(_context.array_h);
    free(_context.matrix_h);
}

template<typename NumRepType, typename HandlerType>
void XgpuHandler<NumRepType, HandlerType>::init(RawBytes<char>& block)
{
    _handler.init(block);
}

template<typename NumRepType, typename HandlerType>
void XgpuHandler<NumRepType, HandlerType>::operator()(RawBytes<NumRepType>& block)
{   
  
    BOOST_LOG_TRIVIAL(info) << "Running xGPU ....."; 
    int xgpu_error = 0;
    long npol;
    //long nstation;
    long nfrequency,ntime;

    npol = _info.npol;
    //nstation = _info.nstation;
    nfrequency = _info.nfrequency;
    ntime = _info.ntime;
    std::size_t count = (std::size_t) _tsamples/ntime;
    std::size_t nelements = _tsamples*nfrequency*_nantennas*npol;

    // Allocation host memory
    BOOST_LOG_TRIVIAL(info) << "Allocating memory...";
    std::vector<ComplexInput> input(nelements);
    std::vector<Complex> output(count * _info.matLength);

    std::uint32_t ind=0;
    auto it = output.begin();

    try
    {
        for ( std::size_t ii = 0 ; ii < nelements; ++ii)
        {
            input[ii].real = static_cast<char>(block.ptr()[ind]);
            input[ii].imag = static_cast<char>(block.ptr()[ind+1]);
            ind += 2;
        }

        for (std::size_t ii=0; ii<count; ++ii)
        {
            /* Memset array_h for each run */
            std::memset(_context.array_h, 0, _info.vecLength);
            std::size_t index = 0;
            std::size_t start = ii*ntime*_nantennas*npol*nfrequency ;
            std::size_t stop = start + ntime*_nantennas*npol*nfrequency;

            assert(stop <= nelements);
            for (std::size_t jj=0; jj< (std::size_t) ntime; ++jj)
            {
                for(std::size_t kk=0; kk < (std::size_t) nfrequency; ++kk)
                {
                    for(std::size_t ll=0; ll < (std::size_t) _info.nstation*npol; ++ll)
                    {
                        if (ll < (std::size_t) npol*_nantennas)
                            _context.array_h[index] = input[start + kk* _nantennas*npol + jj*nfrequency*_nantennas*npol +  ll];
                        ++index;
                    }
                }
            }
            //Call xGPU
            xgpu_error = xgpuCudaXengine(&_context, SYNCOP_DUMP);
            // Check for errors
            if(xgpu_error)
            {
                BOOST_LOG_TRIVIAL(error) << "xgpuCudaXengine returned error code: " <<  xgpu_error;
                throw;
            }
            BOOST_LOG_TRIVIAL(info) << "xGPU ran successfully...";
            // Averaging the values with number of time samples integrated
            assert(_info.matLength <= nelements);

            for (std::uint32_t jj=0;jj < _info.matLength; ++jj)
            {
                _context.matrix_h[jj].real = (float) _context.matrix_h[jj].real/_info.ntime;
                _context.matrix_h[jj].imag = (float) _context.matrix_h[jj].imag/_info.ntime;
            }

            std::copy(_context.matrix_h, _context.matrix_h + _info.matLength, it);
            it += _info.matLength;
            xgpuClearDeviceIntegrationBuffer(&_context);
        }
        BOOST_LOG_TRIVIAL(info) << "Creating Raw Bytes Object..";
        RawBytes<Complex> correlations( output.data(), _info.matLength * count * sizeof(Complex), 0, false);
        _handler(correlations);

    }

    catch (...)
    {
        throw;
    }

}

