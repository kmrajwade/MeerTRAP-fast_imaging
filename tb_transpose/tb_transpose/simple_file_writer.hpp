#ifndef TB_TRANSPOSE_SIMPLE_FILE_WRITER_HPP
#define TB_TRANSPOSE_SIMPLE_FILE_WRITER_HPP

#include "tb_transpose/common.hpp"
#include "tb_transpose/raw_bytes.hpp"
#include "tb_transpose/common.hpp"
#include <fstream>
#include <iomanip>

namespace tb_transpose {

    class SimpleFileWriter
    {
        public:
            explicit SimpleFileWriter(std::string filename, std::uint32_t nelements);
            SimpleFileWriter(SimpleFileWriter const&) = delete;
            ~SimpleFileWriter();
            void init(RawBytes<char>&);
            void init(RawBytes<char>&, std::size_t);

            template<typename NumRepType>
            void operator()(RawBytes<NumRepType>& block );

        private:
            std::ofstream _outfile;
            std::uint32_t _nelements;
    };
}//psrdada_cpp
#include "tb_transpose/detail/simple_file_writer.cpp"
#endif //TB_TRANSPOSE_SIMPLE_FILE_WRITER_HPP
