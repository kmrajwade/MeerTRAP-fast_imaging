#include "tb_transpose/simple_file_writer.hpp"
#include <string>
#include <sstream>

namespace tb_transpose {

    SimpleFileWriter::SimpleFileWriter(std::string filename, std::uint32_t nelements)
    : _nelements(nelements)
    {
        _outfile.open(filename.c_str(),std::ifstream::out | std::ifstream::binary);
        if (_outfile.is_open())
        {
            BOOST_LOG_TRIVIAL(debug) << "Opened file " << filename;
        }
        else
        {
            std::stringstream stream;
            stream << "Could not open file " << filename;
            throw std::runtime_error(stream.str().c_str());
        }
    }

    SimpleFileWriter::~SimpleFileWriter()
    {
        _outfile.close();
    }

    void SimpleFileWriter::init(RawBytes<char>& block)
    {
        _outfile.write(block.ptr(), 4096);
    }

    void SimpleFileWriter::init(RawBytes<char>& block, std::size_t size)
    {
        _outfile.write(block.ptr(), size);
    }

} //psrdada_cpp
