#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include "xgpu.h"
#include "xgpu_info.h"
#include "boost/algorithm/string.hpp"
#include "tb_transpose/FileReader.hpp"
#include "tb_transpose/simple_file_writer.hpp"
#include "tb_transpose/cutt_handler.hpp"
#include "boost/program_options.hpp"

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2; 
}


using namespace tb_transpose;

int main(int argc, char* argv[])
{
    try
    {
        std::string size_string;
        std::string trans_string;
        std::string outfilename;
        std::string infilename;
        std::uint32_t tsamples;
        std::uint32_t nantennas;
        std::uint32_t nfreq;
        std::uint32_t npol;

        /**
         * Define and parse the program options
         */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
            ("help,h", "Print help messages")

            ("nantenna,a", po::value<std::uint32_t>(&nantennas),
             "Number of antennas")

            ("nchannels,f", po::value<std::uint32_t>(&nfreq),
             "number of frequency channels")

            ("npol,p", po::value<std::uint32_t>(&npol),
             "Number of Polarizations")


            ("size,s", po::value<std::string>(&size_string)
             ->default_value("0,0,0,0"),
             "Size of each dimension to be transposed (separated by 'x')")

            ("direction,d", po::value<std::string>(&trans_string)->required(),
             "direction of transpose (comma separated list of N dimensions in order of transpose)")

            ("output,o", po::value<std::string>(&outfilename)->required(),
             "Name of the output file")

            ("input,i", po::value<std::string> (&infilename)->required(),
             "Name of the input file")

            ("time_samples,t", po::value<std::uint32_t> (&tsamples)->required(),
             "Number of time samples to process");



        /* Catch Error and program description */
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "TransientBufferTranspose: Read MeerKAT channelized voltage data and transpose it to whatever format you want."
                    << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)

        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        // CLI app over here

        auto nelements = nantennas * nfreq * npol * tsamples * 2;

        SimpleFileWriter writer(outfilename, nelements);

        CuttHandler<int8_t, decltype(writer)> cutt(writer, size_string, trans_string, nelements);

        FileReader<int8_t, decltype(cutt)> reader(infilename, cutt, nelements);

        reader.start();

        return(0);

    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
