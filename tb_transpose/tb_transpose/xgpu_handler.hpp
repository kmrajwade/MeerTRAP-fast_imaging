#ifndef TB_TRANSPOSE_XGPU_HANDLER_HPP
#define TB_TRANSPOSE_XGPU_HANDLER_HPP

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <glob.h>
#include <string.h>
#include "xgpu.h"
#include "xgpu_info.h"
#include "raw_bytes.hpp"
#include "common.hpp"

// A templated binary file reader irrespective of type of data.


namespace tb_transpose
{

template<typename NumRepType, typename HandlerType>
class XgpuHandler
{
public:
    XgpuHandler( HandlerType& handler, std::uint32_t tsamples, std::uint32_t nantennas); // create array based on file with this name
	~XgpuHandler(void);

    void init(RawBytes<char>& block);

    void operator()(RawBytes<NumRepType>& block);


private:
    HandlerType& _handler;
    std::uint32_t _tsamples;
    std::uint32_t _nantennas;
    XGPUContext _context;
    XGPUInfo _info;

};
}
#include "tb_transpose/detail/xgpu_handler.cpp"
#endif
