#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <glob.h>
#include <string.h>
#include "xgpu.h"
#include "xgpu_info.h"

/* A test code using xGPU for correlating MeerKAT data for demonstration and benchmarking*/
/* Kaustubh Rajwade */
/* 01 May 2018 */

/*
Data ordering for input vectors is (running from slowest to fastest)
[time][channel][station][polarization][complexity]
 
Output matrix has ordering
[channel][station][station][polarization][polarization][complexity]
*/

/* Function to generate data */

void gen_data(ComplexInput* in_data, long time, long nchans, long nstations, long npol)
{
    long ii,jj,kk,ll;
    long ind = 0;
    int val=1;
    for (ii=0; ii < time; ++ii)
    {
        for (jj=0; jj < nchans; ++jj)
        {
            for (kk=1; kk <=nstations; ++kk)
            {
                for (ll=0; ll < npol; ++ll)
                {
                    in_data[ind].real = (char) val * kk;
                    in_data[ind].imag = (char) 0;
                    ind+=1;
                }
                if (val == 1)
                {
                    val = -1;
                }
                else
                {
                    val = 1;
                }
            }
        }
    }
}


/* Main code */

int main(int argc, char** argv) {

  int opt;
  int i, j;
  int device = 0;
  int syncOp = SYNCOP_SYNC_TRANSFER;
  int finalSyncOp = SYNCOP_DUMP;
  int verbose = 0;
  int hostAlloc = 0;
  XGPUInfo xgpu_info;
  long npol, nstation, nfrequency,ntime;
  int xgpu_error = 0;
  Complex *omp_matrix_h = NULL;
  unsigned char *elem = NULL; 
  char path[1024];
  glob_t globbuf;

  // Put in help

  if (argc < 2)
  {
      fprintf(stderr,"Error: wrong usage/incorrect arguments\n");
      fprintf(stderr,"Usage: xgpu_data_verification <name of output file>\n");
      exit(1); 
  }


  // Getting array dimensions
 
  xgpuInfo(&xgpu_info);
  npol = xgpu_info.npol;
  nstation = xgpu_info.nstation;
  nfrequency = xgpu_info.nfrequency;
  ntime = xgpu_info.ntime;

  long size=0;

  XGPUContext context;
  context.array_h = NULL;
  context.matrix_h = NULL;

 
 // Initializing xGPU
  xgpu_error = xgpuInit(&context, device);
  if(xgpu_error) {
    fprintf(stderr, "xgpuInit returned error code %d\n", xgpu_error);
    goto cleanup;
  }

  printf("xGPU init done....\n");
 
  // generating data
  gen_data(context.array_h,ntime,nfrequency,nstation,npol);

// Opening output file

  FILE *ofptr=NULL;
  ofptr= fopen(argv[1], "wb");
  if (ofptr == NULL)
  {
     fprintf(stderr,"Error opening output file.\n");
     exit(1);
  }

// Put in a clock for benchmarking
 
  printf("Start correlation....\n");
  //Call xGPU
  xgpu_error = xgpuCudaXengine(&context, finalSyncOp);
  // Check for errors
  if(xgpu_error) 
  {
      fprintf(stderr, "xgpuCudaXengine returned error code %d\n", xgpu_error);
      goto cleanup;
  }
  // Averaging the values with number of time samples integrated
  for (j=0;j < xgpu_info.matLength; ++j)
  {
      context.matrix_h[j].real = (float)context.matrix_h[j].real/xgpu_info.ntime;
      context.matrix_h[j].imag = (float)context.matrix_h[j].imag/xgpu_info.ntime;
  }
  fwrite(context.matrix_h,sizeof(Complex),xgpu_info.matLength,ofptr);
  xgpuClearDeviceIntegrationBuffer(&context); 


/* Normal clean up */
cleanup:
 // free gpu memory
  xgpuFree(&context);
  
  if(hostAlloc) {
    free(context.array_h);
    free(context.matrix_h);
  }                    
  return xgpu_error;
}
  
 
