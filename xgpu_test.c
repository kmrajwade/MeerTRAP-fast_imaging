#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <glob.h>
#include <string.h>
#include "xgpu.h"
#include "xgpu_info.h"

/* A test code using xGPU for correlating MeerKAT data for demonstration and benchmarking*/
/* Kaustubh Rajwade */
/* 01 May 2018 */

/*
Data ordering for input vectors is (running from slowest to fastest)
[time][channel][station][polarization][complexity]
 
Output matrix has ordering
[channel][station][station][polarization][polarization][complexity]
*/

/* Main code */

int main(int argc, char** argv) {

  int opt;
  int i, j;
  int device = 0;
  int syncOp = SYNCOP_SYNC_TRANSFER;
  int finalSyncOp = SYNCOP_DUMP;
  int verbose = 0;
  int hostAlloc = 0;
  XGPUInfo xgpu_info;
  long npol, nstation, nfrequency,ntime;
  int xgpu_error = 0;
  Complex *omp_matrix_h = NULL;
  unsigned char *elem = NULL; 
  char path[1024];
  glob_t globbuf;

  // Put in help

  if (argc < 5)
  {
      fprintf(stderr,"Error: wrong usage/incorrect arguments\n");
      fprintf(stderr,"Usage: xgpu_test <pathto the raw files> <name of output file> <seconds to skip> <seconds to process>\n");
      exit(1); 
  }

 /* Parsing the correct arguments*/
  float startsec = atof(argv[3]);
  float endsec = startsec + (atof(argv[4]));

 /* Globbing all the files*/

  sprintf(path,"%s",argv[1]);

 // make sure that you only have the required dada files in the path
  strcat(path,"*.dada");
  glob(path,GLOB_ERR,NULL,&globbuf);

  if(globbuf.gl_pathv == NULL)
  {
      fprintf(stderr,"Error: Files not found. Make sure you have the correct path.\n");
      exit(1);
  }


  // Getting array dimensions
 
  xgpuInfo(&xgpu_info);
  npol = xgpu_info.npol;
  nstation = xgpu_info.nstation;
  nfrequency = xgpu_info.nfrequency;
  ntime = xgpu_info.ntime;

  long size=0;

  // get the size
  FILE *fptr = fopen(globbuf.gl_pathv[0],"rb");
  fseek(fptr,0,SEEK_END);
  size = ftell(fptr) - 4096 ;
  fclose(fptr);

 // Allocation host memory
  int tsamp = size/(npol*nfrequency*2);
  XGPUContext context;
  context.array_h = NULL;
  context.matrix_h = NULL;
  elem = (unsigned char*)malloc(size*nstation * sizeof(unsigned char));
  memset(elem,0,size*nstation*sizeof(unsigned char));

  // Parsing binary data from dada files
  printf("Reading data into arrays....\n");
 
  FILE *fptr1[nstation];
  int index=0;
 
  // This loop expects nstations number of dada files, if there are less, change the upper limit on the for loop

  for (i=0; i<nstation;i++)
  {
    fptr1[i] = fopen(globbuf.gl_pathv[i],"rb");
    printf("File: %s\n",globbuf.gl_pathv[i]);
    if (fptr1[i] == NULL)
    {
        fprintf(stderr,"Error:File not opened!\n");
        exit(1);
    }
    fseek(fptr1[i],4096,SEEK_SET);
    fread(elem+(i*size),sizeof(unsigned char),size,fptr1[i]);
    fclose(fptr1[i]);
  }  

 // Initializing xGPU
  xgpu_error = xgpuInit(&context, device);
  if(xgpu_error) {
    fprintf(stderr, "xgpuInit returned error code %d\n", xgpu_error);
    goto cleanup;
  }

  printf("xGPU init done....\n");
  //set up total number of iterations of xGPU
  
  // Calculate how many iterations through the data
  int start_tsamp = (int) startsec/0.00000478;
  int end_tsamp = (int) endsec/0.00000478;
  int start_count = start_tsamp/ntime;
  int end_count = end_tsamp/ntime;
  int count = tsamp/ntime;

  long ii, jj, kk, ll, mm;

  // Merging files to the right format

  /* Currently using two methods to transpose: One is faster and the other is memory efficient (without using GPUs).*/
 
  /* Method1: Use more RAM and make faster transpose*/

  ComplexInput *transposed= NULL;
  transposed = (ComplexInput*)malloc(tsamp*nfrequency*nstation*npol*sizeof(ComplexInput));
  long ind=0;
  printf("Rearranging data in files.....\n");
  for (ii=0 ; ii < tsamp ; ii++)
  {

    for (jj=0;jj < nfrequency ; jj++)
    {
      for (kk=0; kk < nstation ;kk++)
      {
       for (ll=0; ll < npol;ll++)
       {        
            transposed[ind].real = (char) elem[ll*npol + (kk*nfrequency*tsamp*npol*2) + (jj*npol*2) + (ii*nfrequency*npol*2)] ;
            transposed[ind].imag = (char) elem[ll*npol + (kk*nfrequency*tsamp*npol*2) + (jj*npol*2) + (ii*nfrequency*npol*2) + 1];
            ind +=1;
       }
      }
    }
  }

  free(elem); 
  /* Method2: Use less RAM by a factor of 2 but a lot more nested loops */

  // TBD
  
// Opening output file

  FILE *ofptr=NULL;
  ofptr= fopen(argv[2], "wb");
  if (ofptr == NULL)
  {
     fprintf(stderr,"Error opening output file.\n");
     exit(1);
  }

  printf("Skipping %f seconds of data and processing %f seconds...\n",startsec,atof(argv[4]));
// Put in a clock for benchmarking
 
  printf("Start correlation....\n");
  clock_t begin=clock();  
  long start=0, stop=0,a;
  for (ii=start_count; ii<end_count; ii++)
  {
     a = 0;
     start = ii*xgpu_info.vecLength;
     stop = start + xgpu_info.vecLength;
   
     printf("Running integration:  %d ....\n",ii);
     for (jj=start; jj<stop; jj++)
     {
        context.array_h[a] = transposed[jj];
        a+=1;
     }
     //Call xGPU
     xgpu_error = xgpuCudaXengine(&context, finalSyncOp);
     // Check for errors
     if(xgpu_error) 
     {
        fprintf(stderr, "xgpuCudaXengine returned error code %d\n", xgpu_error);
        goto cleanup;
     }
     // Averaging the values with number of time samples integrated
     for (jj=0;jj < xgpu_info.matLength; ++jj)
     {
        context.matrix_h[jj].real = context.matrix_h[jj].real/xgpu_info.ntime;
        context.matrix_h[jj].imag = context.matrix_h[jj].imag/xgpu_info.ntime;
     }
     fwrite(context.matrix_h,sizeof(Complex),xgpu_info.matLength,ofptr);
     xgpuClearDeviceIntegrationBuffer(&context); 
  }

/* Write down some stats */
  clock_t end=clock();
  float total = ((double)(end - begin) / (CLOCKS_PER_SEC))*1000;
  float per_call = total/count;

  float max_bw = xgpu_info.ntime*xgpu_info.nfrequency/per_call/1000; // MHz
  float gbps = ((float)(8 * context.array_len * sizeof(ComplexInput) * count)) / total / 1e6; // Gbps
  printf("Elapsed time %.6f ms total, %.6f ms/call average\n",
        total, per_call);
  printf("Theoretical BW_max %.3f MHz, throughput %.3f Gbps\n",
        max_bw, gbps);

  // Write the output to some file
  
  double time_spent = (double)(end - begin) / (CLOCKS_PER_SEC);
  fclose(ofptr);


/* Normal clean up */

  free(transposed);

cleanup:
 // free gpu memory
  xgpuFree(&context);
  
  if(hostAlloc) {
    free(context.array_h);
    free(context.matrix_h);
  }                    
  return xgpu_error;
}
  
 
