#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <string.h>
#include "xgpu.h"
#include "xgpu_info.h"

/* A test code using xGPU for correlating random noise data for demonstration and benchmarking*/
/* Kaustubh Rajwade */
/* 01 May 2018 */

/*
Data ordering for input vectors is (running from slowest to fastest)
[time][channel][station][polarization][complexity]
 
Output matrix has ordering
[channel][station][station][polarization][polarization][complexity]
*/

/* Main code */

int main(int argc, char** argv) {

  int opt;
  int i, j;
  int device = 0;
  int syncOp = SYNCOP_SYNC_TRANSFER;
  int finalSyncOp = SYNCOP_DUMP;
  int verbose = 0;
  int hostAlloc = 0;
  XGPUInfo xgpu_info;
  long npol, nstation, nfrequency;
  int xgpu_error = 0;
  Complex *omp_matrix_h = NULL;
  unsigned char *elem = NULL; 

  // Put in help

  if (argc < 3)
  {
      fprintf(stderr,"Error: wrong usage/incorrect arguments\n");
      fprintf(stderr,"Usage: xgpu_test <data file name> <name of output file>\n");
      exit(1); 
  }

 /* Globbing all the files*/

  xgpuInfo(&xgpu_info);
  npol = xgpu_info.npol;
  nstation = xgpu_info.nstation;
  nfrequency = xgpu_info.nfrequency;
  ntime = xgpu_info.ntime;

  long size=0;

  // get the size
  FILE *fptr1 = NULL;
  FILE *fptr = fopen(argv[1],"rb");
  fseek(fptr,0,SEEK_END);
  size = ftell(fptr);
  fclose(fptr);

 // Allocation host memory
  int tsamp = size/(nstation*npol*nfrequency*2);
  XGPUContext context;
  //context.array_len = xgpu_info.vecLength;
  //context.matrix_len =xgpu_info.matLength ;
  context.array_h = NULL;
  context.matrix_h = NULL;
  elem = (unsigned char*)malloc(size * sizeof(unsigned char));

  // Parsing binary data from dada files
  printf("Reading data into arrays....\n");
  
  fptr1 = fopen(argv[1],"rb");
  fread(elem,sizeof(unsigned char),size,fptr1);
  fclose(fptr1);

 // Initializing xGPU
  xgpu_error = xgpuInit(&context, device);
  if(xgpu_error) {
    fprintf(stderr, "xgpuInit returned error code %d\n", xgpu_error);
    goto cleanup;
  }

  printf("xGPU init done....\n");
  //set up total number of iterations of xGPU
  int count = tsamp/500;

  long ii, jj, kk, ll, mm;
  
// Opening output file

  FILE *ofptr=NULL;
  ofptr= fopen(argv[2], "wb");
  if (ofptr == NULL)
  {
     fprintf(stderr,"Error opening output file.\n");
     exit(1);
  }

// Put in a clock for benchmarking
 
  printf("Start correlation....\n");
  clock_t begin=clock();  
  long start=0, stop=0,a;
  for (ii=0; ii<count; ii++)
  {
     a = 0;
     start = ii*xgpu_info.vecLength*2;
     stop = start + 2*xgpu_info.vecLength;
   
     printf("Running integration:  %d ....\n",ii);
     for (jj=start; jj<stop; jj+=2)
     {
        context.array_h[a].real = elem[jj];
        context.array_h[a].imag = elem[jj+1];
        a+=1;
     }
     //Call xGPU
     xgpu_error = xgpuCudaXengine(&context, finalSyncOp);
     // Check for errors
     if(xgpu_error) {
        fprintf(stderr, "xgpuCudaXengine returned error code %d\n", xgpu_error);
        goto cleanup;
      }
     fwrite(context.matrix_h,sizeof(Complex),xgpu_info.matLength,ofptr);
  }

/* Write down some stats */
  clock_t end=clock();
  float total = ((double)(end - begin) / (CLOCKS_PER_SEC))*1000;
  float per_call = total/count;

  float max_bw = xgpu_info.ntime*xgpu_info.nfrequency/per_call/1000; // MHz
  float gbps = ((float)(8 * context.array_len * sizeof(ComplexInput) * count)) / total / 1e6; // Gbps
  printf("Elapsed time %.6f ms total, %.6f ms/call average\n",
        total, per_call);
  printf("Theoretical BW_max %.3f MHz, throughput %.3f Gbps\n",
        max_bw, gbps);

  // Write the output to some file
  
  double time_spent = (double)(end - begin) / (CLOCKS_PER_SEC);
  fclose(ofptr);


/* Normal clean up */

cleanup:
 // free gpu memory
  xgpuFree(&context);
  
  if(hostAlloc) {
    free(context.array_h);
    free(context.matrix_h);
  }                    
  return xgpu_error;
}
  
 
